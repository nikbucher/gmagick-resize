# Stage 1: Build the application
FROM golang:1.21 AS builder

# Update package lists and install necessary packages
RUN apt update && \
    apt install -y libgraphicsmagick1-dev

WORKDIR /app
COPY . .

# Build the Go application
RUN go build -o gmagick-resize .

# Stage 2: Create a minimal runtime image
FROM ubuntu:latest

# Install necessary packages
RUN apt update && \
    apt install -y libgraphicsmagick1-dev && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

# Add non-root user
ARG USERNAME=non-root
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME && \
    useradd --uid $USER_UID --gid $USER_GID -m $USERNAME

# Set the default user
USER $USERNAME

WORKDIR /app

# Copy the built executable from the previous stage
COPY --from=builder /app/gmagick-resize .

# Set environment variables
ENV GIN_MODE=release

# Expose port 8080
EXPOSE 8080

# Define the entrypoint for the application
ENTRYPOINT ["./gmagick-resize"]
