package main

import (
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"os"
	"strconv"
)

func ResizeImageHandler(c *gin.Context) {
	// Parse form data, including image file
	file, err := c.FormFile("image")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Image file is required"})
		return
	}

	// Parse the long edge size from the URL parameter
	longEdge, err := strconv.Atoi(c.Param("longEdge"))
	if err != nil || longEdge <= 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid long edge size"})
		return
	}

	// Parse the target image type (optional)
	targetType := c.Query("type")

	// Create a temporary file to store the input image
	inputFile, err := os.CreateTemp("", "input_image_*")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to create temporary input file"})
		return
	}
	defer os.Remove(inputFile.Name())

	// Save the uploaded file to the temporary input file
	uploadedFile, err := file.Open()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to read uploaded file"})
		return
	}
	defer uploadedFile.Close()

	_, err = io.Copy(inputFile, uploadedFile)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to save uploaded file"})
		return
	}

	// Create a temporary file to store the resized image
	outputFile, err := os.CreateTemp("", "resized_image_*")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to create temporary output file"})
		return
	}
	defer os.Remove(outputFile.Name())

	// Resize the image
	err = ResizeImage(inputFile.Name(), outputFile.Name(), uint(longEdge), targetType)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Return the resized image
	c.File(outputFile.Name())
}

func VersionHandler(c *gin.Context) {
	version, _ := Version()
	c.Writer.WriteString(version + "\n")
}
