package main

import (
	"github.com/gographics/gmagick"
)

func ResizeImage(inputPath, outputPath string, longEdge uint, targetType string) error {
	wand := gmagick.NewMagickWand()
	defer wand.Destroy()

	if err := wand.ReadImage(inputPath); err != nil {
		return err
	}

	// Calculate the aspect ratio
	width := wand.GetImageWidth()
	height := wand.GetImageHeight()

	var newWidth, newHeight uint

	if width > height {
		newWidth = longEdge
		newHeight = uint(float64(height) * (float64(longEdge) / float64(width)))
	} else {
		newWidth = uint(float64(width) * (float64(longEdge) / float64(height)))
		newHeight = longEdge
	}

	if err := wand.ResizeImage(newWidth, newHeight, gmagick.FILTER_LANCZOS, 1); err != nil {
		return err
	}

	// If targetType is specified, set the image format
	if targetType != "" {
		if err := wand.SetImageFormat(targetType); err != nil {
			return err
		}
	}

	if err := wand.WriteImage(outputPath); err != nil {
		return err
	}

	return nil
}

func Version() (string, uint) {
	return gmagick.GetVersion()
}
