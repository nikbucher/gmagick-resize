# Image Resizer Service

This is a simple Go application that provides an API endpoint for resizing images.

## Usage

### Endpoint

- `POST /resize/:longEdge`

### Request

- **Method:** POST
- **Path:** `/resize/:longEdge`
- **Parameters:**
	- `longEdge` (int): The desired long edge size for resizing the image.
	- `type` (string, optional): The target image type (e.g., jpg, png).

#### Example Request

```bash
curl -X POST -F "image=@path_to_input_image.jpg" http://localhost:8080/resize/800?type=png > resized_image.png
```

### Response

- **Status Codes:**
	- `200 OK`: Image resized successfully.
	- `400 Bad Request`: Invalid request (e.g., missing image file).
	- `500 Internal Server Error`: An error occurred while processing the request.

### Error Handling

- If an error occurs during image processing, the server will respond with a JSON object containing an error message.

#### Example Error Response

```json
{
	"error": "Invalid long edge size"
}
```

## Running the Application

### Docker

1. Build the Docker image:

	 ```bash
	 docker build -t gmagick-resize .
	 ```

2. Run the Docker container:

	 ```bash
	 docker run -p 8080:8080 gmagick-resize
	 ```

### Local Development

1. Install Go and required dependencies:

	 ```bash
	 go get github.com/gin-gonic/gin
	 go get github.com/gographics/gmagick
	 ```

2. Run the application:

	 ```bash
	 go run main.go
	 ```

## Tests

To run tests, use the following command:

```bash
go test
```

---

Please make sure to replace placeholders like `path_to_input_image.jpg`, `gmagick-resize`, and any other placeholders with your actual project-specific details.

Additionally, if you have any specific instructions or setup steps not covered here, be sure to include them in the README as well.
