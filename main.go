package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.POST("/resize/:longEdge", ResizeImageHandler)
	router.GET("/version", VersionHandler)
	fmt.Println("Server is running at http://localhost:8080")
	router.Run(":8080")
}
